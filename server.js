const express = require('express');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema');
const mongoose = require('mongoose');
const app = express();
const cors = require('cors');

mongoose.connect(
  'mongodb+srv://conor_9tails:Mongoatlas123@cluster0.hz75r.mongodb.net/graphql-netninja-tutorial?retryWrites=true&w=majority',
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useUnifiedTopology: false, // sau 30000ms mongo server sẽ tự động đóng
    useFindAndModify: false,
    useUnifiedTopology: true,
  }
);

mongoose.Promise = global.Promise;

mongoose.connection.on('connected', () => {
  console.log('database: graphql-netninja-tutorial');
});

app.use(cors());
app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(4000, function () {
  console.log('done');
});
